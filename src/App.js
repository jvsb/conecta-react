import Logo from './image/header/marca-conecta-03 1.png';
import IG from './image/header/instagram (1).png';
import Face from './image/header/facebook.png';
import LataHeader from './image/chamativo/lata-guarana1.png';
import Garrafa1 from './image/produtos/garrafa-guarana1.svg';
import Garrafa2 from './image/produtos/lata-guarana2.png';
import Img2Chamativo2 from './image/chamativo2/ash-unsplash 1.png';
import Img1Chamativo2 from './image/chamativo2/michael-unsplash (1) 1.svg';
import Lata1 from './image/experimentar/lata-guarana3.png';
import Lata2 from './image/experimentar/lata-guarana4.png';
import Lata3 from './image/experimentar/lata-guarana5.png';
import RS from './image/faleconosco/RS.png';
import LogoFoot from './image/faleconosco/Grupo 38 (1).png';
import IGFoot from './image/faleconosco/instagram (1).png';
import FaceFoot from './image/faleconosco/facebook.png';
import './App.css';


function App() {
    var slideIndex = 1;
showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
        }

    function currentSlide(n) {
        showSlides(slideIndex = n);
        }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        console.log(slides.style);
        var dots = document.getElementsByClassName("dot");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
                }
            for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
            }
}
  return (
      <main>
        <div id="chamativo1">
        <div id="header-conecta">
            <img id="header-logo" src={ Logo } alt="logo conecta"/>
                <nav id="redes" alt="redes sociais">
                <img id="header-fonts" src={ IG } alt="logo facebook"/>
                <img id="header-fonts" src={ Face } alt="logo instagram"/>
                </nav>
        </div>
            <div id="textobuttondiv">
            <div>
                <h1 id="titulo1" alt="titulo chamativo1">Aqui um título de duas linhas</h1>
                <button id="buttondiv" alt="botao compre já">Compre já!</button>
            </div>
            <div id="div-img1">
                <img id="img-chamativa1" src={ LataHeader } alt="lata gauarana"/>
            </div>
            </div>
        </div>
        <div id="produtos">
        <div id="prodh1p">
            <h1>Produtos</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus egestas erat vitae mauris tincidunt.</p>
        </div>
        <div id="produtos-container">
            <div id="produtos-div">
                <img id="img-produto1" src={ Garrafa1 } alt="garrafa guarana"/>
                <h1>Garrafa 350ml</h1>
                <p>Lorem ipsum dolor sit ametus casus</p>
                <button>Compre já!</button>
            </div>
            <div id="produtos-div">
                <img id="img-produto2" src={ Garrafa2 } alt="lata guarana"/>
                <h1>Lata 100ml</h1>
                <p>Lorem ipsum dolor sit amet</p>
                <button>Compre já!</button>
            </div>
        </div>
    </div>
    <div id="chamativo2">
            <div id="img1-chamativo2">
             <img src={ Img1Chamativo2 } alt="imagem de bebida"/>
        </div>
        <div class="slideshow-container">
            <div class="mySlides" id="firstSlide">
                <q>Lorem ipsum dolor sit ametas, consecteturas adipiscing elitas. Phasellus egestas erat vitae mauris tincidunt.</q>
                <p class="author">@guest1</p>
            </div>
            <div class="mySlides">
                <q>Lorem ipsum dolor sit ametes, consectetures adipiscing elites. Phasellus egestas erat vitae mauris tincidunt.</q>
                <p class="author">@guest2</p>
            </div>
            <div class="mySlides">
                <q>Lorem ipsum dolor sit ametis, consecteturis adipiscing elitis. Phasellus egestas erat vitae mauris tincidunt.</q>
                <p class="author">@guest3</p>
            </div>
            <div class="mySlides">
                <q>Lorem ipsum dolor sit ametus, consecteturus adipiscing elitus. Phasellus egestas erat vitae mauris tincidunt.</q>
                <p class="author">@guest4</p>
            </div>
            <div class="dot-container">
                <span class="dot" onclick={currentSlide(1)}></span>
                <span class="dot" onclick={currentSlide(2)}></span>
                <span class="dot" onclick={currentSlide(3)}></span>
                <span class="dot" onclick={currentSlide(4)}></span>
            </div>
        </div>
        <div id="img2-chamativo2">
            <img src={ Img2Chamativo2 } alt="imagem com texto"/>
            <h1>Peça para seu bar</h1>
        </div>
    </div>
    <div id="experimente">
        <img id="expimg1" src={ Lata1 } alt="lata-guaraná 3"/>
        <img id="expimg2" src={ Lata2 } alt="lata-guaraná 4"/>
        <div id="h1button-experimenta">
            <h1>Quer experimentar?</h1>
            <button>Compre já</button>
        </div>
        <img id="expimg3" src={ Lata3 } alt="lata-guaraná 5"/>
    </div>
    <div id="faleconosco">
        <div id="txtfaleconosco">
            <h1>Fale Conosco.</h1>
            <nav id="redes-foot">                
                <img id="footer-fonts" src={ FaceFoot } alt="logo facebook"/>
                <img id="footer-fonts" src={ IGFoot } alt="logo instagram"/>
                <img src={ RS } alt="chamativo para redes"/>
            </nav>
        </div>
        <div id="formulario">
            <form>
                <div id="wrapper">
                    <div id="box">
                        <input type="text" name="fname" id="fname" placeholder="João Vitor" required/>
                        <label for="fname">NOME</label>
                    </div>
                </div>
                <div id="wrapper">
                    <div id="box">
                        <input type="text" name="tel" id="tel" placeholder="(21)00000-0000" required/>
                        <label for="tel">TELEFONE</label>
                    </div>
                    <div id="box">
                        <input type="email" name="mail" id="mail" placeholder="email@email.com.br" required/>
                        <label for="mail">EMAIL</label>
                    </div>
                </div>
                <div id="wrapper">
                    <div id="box">
                        <label>ASSUNTO</label>
                        <select id="assunto">
                            <option disabled selected>Escolha um assunto</option>
                            <option>Bebidas</option>
                            <option>Comercial</option>
                            <option>Outros</option>
                        </select>
                    </div>
                </div>
                <div id="wrapper">
                    <div id="box">
                        <textarea id="mes" placeholder="Escreva aqui sua mensagem" required></textarea>
                        <label for="mes">MENSAGEM</label>
                    </div>
                </div>
                <input type="submit" name="" value="Enviar"/>
            </form>
        </div>
            <div id="direitosfoot">
            <h3>Todos os direitos reservados © 2020 Conecta</h3>
            </div>
            <div id="direitosfoot2">
            <h4>Projetado por</h4><img src={ LogoFoot }/>
            </div>
    </div>
    </main>
  );
}

export default App;
