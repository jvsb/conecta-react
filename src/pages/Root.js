import Header from './header/Header'
import Principal from './principal/Principal'
import Footer from './footer/Footer'


const Root = () => {
    return(
        <div>
        <Principal/>
        <Footer/>
        </div>
    );
}
export default Root;
