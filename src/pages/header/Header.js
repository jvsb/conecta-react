import React from 'react';
import './Header.css';
import Logo from '../../image/header/marca-conecta-03 1.png';
import IG from '../../image/header/instagram (1).png';
import Face from '../../image/header/facebook.png';


function Header () {

  return (
        <header>
            <div id="header">
                <img id="header-logo" src={ Logo } alt="logo conecta"/>
                <nav id="redes" alt="redes sociais">
                <img id="header-fonts" src={ IG } alt="logo facebook"/>
                <img id="header-fonts" src={ Face } alt="logo instagram"/>
                </nav>
            </div>
        </header>
  );    
}
export default Header;